module Main exposing (..)

import Browser exposing (element)
import Browser.Dom
import Browser.Events

import Element exposing (..)
import Element.Background as Background

import Html exposing (Html)

import Hex exposing (toString)
import Json.Decode
import Task

afterwork_image : Color -> Element msg
afterwork_image bg_color =
  image
    [ Background.color bg_color, centerX ]
    { src = "images/sketchisthenewclean.png", description = "vodka" }

main =
  Browser.element
    { init = init
    , update = update
    , view = view
    , subscriptions = subscriptions
    }

type alias Model =
  { red : Int
  , green : Int
  , blue : Int
  , width : Int
  , height : Int
  }

type Msg
  = MouseMove Int Int
  | Resize Int Int

init : () -> ( Model, Cmd Msg )
init _ =
  ( Model 100 100 100 0 0
  , Task.perform (
      \{ viewport } ->
        Resize (round viewport.width) (round viewport.height))
        Browser.Dom.getViewport
  )

subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.batch
    [ Browser.Events.onMouseMove
        (Json.Decode.map2 MouseMove
          (Json.Decode.field "pageX" Json.Decode.int)
          (Json.Decode.field "pageY" Json.Decode.int)
        )
    , Browser.Events.onResize Resize
    ]

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    MouseMove x y ->
      ( { model | red = (scale x model.width), green = (scale y model.height) }
      , Cmd.none
      )

    Resize w h ->
      ( { model | width = w, height = h }
      , Cmd.none
      )

intToHexString : Int -> String
intToHexString n =
  let
    hex = Hex.toString n
  in
    case String.length hex of
      1 ->
        String.append "0" hex
      2 ->
        hex
      _ ->
        "ff"

modelToViewportString : Model -> String
modelToViewportString model =
  [model.width, model.height]
  |> List.map String.fromInt
  |> String.join ", "

scale : Int -> Int -> Int
scale x max =
  let
      ratio = (toFloat x) / (toFloat max)
  in
    round (255 * ratio)

-- view : Model -> Html Msg
-- view model =
--   layout [] <|
--     row [height fill, width fill, Background.color <| rgb255 0 0 0]
--         [ afterwork_image <| rgb255 model.red model.green model.blue ]

view : Model -> Html Msg
view model =
  layout [Background.color <| rgb255 0 0 0] <|
    afterwork_image <| rgb255 model.red model.green model.blue
